import sys
from tqdm import tqdm

f = open(sys.argv[1], 'r')

line = [int(el) for el in f.readline().rstrip('\n').split(' ')]

number_of_books = line[0]
number_of_libraries = line[1]
days_limit = line[2]

book_score = [int(el) for el in f.readline().rstrip('\n').split(' ')]

libraries = []

for _ in range(number_of_libraries):
	line1 = f.readline().rstrip('\n').split(' ')
	books_ids = [int(el) for el in f.readline().rstrip('\n').split(' ')]
	data = {
		'number_of_books_in_library': int(line1[0]),
		'signup_time': int(line1[1]),
		'books_per_day': int(line1[2]),
		'books_ids': books_ids
	}
	libraries.append(data)

# Sortiranje biblioteka po velicini

sorted_libraries = sorted(range(len(libraries)), key=lambda i: libraries[i]['number_of_books_in_library'])


# Output

print(number_of_libraries)
for library_index in sorted_libraries:
	print(library_index, libraries[library_index]['number_of_books_in_library'])
	print(' '.join([str(el) for el in libraries[library_index]['books_ids']]))
