import sys
import numpy as np
from tqdm import tqdm

file_number = int(sys.argv[1])

input_files = [
	'a_example.txt',
	'b_read_on.txt',
	'c_incunabula.txt',
	'd_tough_choices.txt',
	'e_so_many_books.txt',
	'f_libraries_of_the_world.txt'
]

output_files = [
	'a' + str(file_number) + '.out',
	'b' + str(file_number) + '.out',
	'c' + str(file_number) + '.out',
	'd' + str(file_number) + '.out',
	'e' + str(file_number) + '.out',
	'f' + str(file_number) + '.out'
]

for file_index, input_file in enumerate(input_files):
	f = open(input_file, 'r')

	line = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	number_of_books = line[0]
	number_of_libraries = line[1]
	days_limit = line[2]

	book_score = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	libraries = []

	for _ in range(number_of_libraries):
		line1 = f.readline().rstrip('\n').split(' ')
		books_ids = [int(el) for el in f.readline().rstrip('\n').split(' ')]
		books_ids = sorted(books_ids, key=lambda i: book_score[i])
		data = {
			'number_of_books_in_library': int(line1[0]),
			'signup_time': int(line1[1]),
			'books_per_day': int(line1[2]),
			'books_ids': books_ids
		}
		libraries.append(data)

	# Sortiranje biblioteka po velicini

	sorted_libraries = sorted(range(len(libraries)), key=lambda i: libraries[i]['number_of_books_in_library'])


	# Output

	outFile = open(output_files[file_index], 'w')
	outFile.write(str(number_of_libraries) + '\n')
	for library_index in sorted_libraries:
		outFile.write(str(library_index) + ' ' + str(libraries[library_index]['number_of_books_in_library']) + '\n')
		outFile.write(' '.join([str(el) for el in libraries[library_index]['books_ids']]) + '\n')
