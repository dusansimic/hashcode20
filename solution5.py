import sys
import numpy as np
import math
from tqdm import tqdm

file_number = int(sys.argv[1])

input_files = [
	'a_example.txt',
	'b_read_on.txt',
	'c_incunabula.txt',
	'd_tough_choices.txt',
	'e_so_many_books.txt',
	'f_libraries_of_the_world.txt'
]

output_files = [
	'a' + str(file_number) + '.out',
	'b' + str(file_number) + '.out',
	'c' + str(file_number) + '.out',
	'd' + str(file_number) + '.out',
	'e' + str(file_number) + '.out',
	'f' + str(file_number) + '.out'
]

for file_index, input_file in enumerate(input_files):
	f = open(input_file, 'r')

	line = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	number_of_books = line[0]
	number_of_libraries = line[1]
	days_limit = line[2]

	book_score = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	libraries = []

	for _ in range(number_of_libraries):
		line1 = f.readline().rstrip('\n').split(' ')
		books_ids = [int(el) for el in f.readline().rstrip('\n').split(' ')]
		books_ids = sorted(books_ids, key=lambda i: book_score[i])
		data = {
			'number_of_books_in_library': int(line1[0]),
			'signup_time': int(line1[1]),
			'books_per_day': int(line1[2]),
			'books_ids': books_ids,
			'books_sum': sum([book_score[book_id] for book_id in books_ids])
		}
		libraries.append(data)

	# Sortiranje biblioteka po velicini

	max_signup_time = max(libraries, key=lambda lib: lib['signup_time'])['signup_time']
	sorted_libraries = sorted(range(len(libraries)), key=lambda i: (max_signup_time - libraries[i]['signup_time'], libraries[i]['books_sum']))

	time_offset = 0
	used_books = []

	for some_index, library_index in tqdm(enumerate(sorted_libraries)):
		new_books_list = np.setdiff1d(libraries[library_index]['books_ids'], used_books) # Uzimanje onih koje nisu skenirane
		libraries[library_index]['books_ids'] = new_books_list # Setovanje onih koje nisu skenirane
		libraries[library_index]['number_of_books_in_library'] = len(new_books_list) # Izmena broja knjiga nakon setovanja

		if time_offset + libraries[library_index]['signup_time'] > days_limit:
			number_of_libraries -= 1
			libraries[library_index]['number_of_books_in_library'] = 0
			continue

		required_time = time_offset + libraries[library_index]['signup_time'] + math.ceil(libraries[library_index]['number_of_books_in_library']/libraries[library_index]['books_per_day']) # Racunanje potrebnog vremena za trenutnu biblioteku

		delta_time = days_limit - required_time
		if delta_time < 0:
			doable_time = math.ceil(libraries[library_index]['number_of_books_in_library']/libraries[library_index]['books_per_day']) + delta_time # Racuanje dozvoljenog vremena za knjige
			doable_books = doable_time * libraries[library_index]['books_per_day'] # Broj knjiga koji se moze izvrsiti
			new_books_list = new_books_list[:doable_books]
			if doable_books == 0:
				number_of_libraries -= 1
				libraries[library_index]['number_of_books_in_library'] = 0
				continue

		if libraries[library_index]['number_of_books_in_library'] == 0:
			number_of_libraries -= 1
		for book_id in new_books_list: # Dodavanje skeniranih knjiga u crnu listu
			used_books.append(book_id)
		time_offset += libraries[library_index]['signup_time'] # Pomeranje offseta za vreme signupovanja

		if some_index + 1 < len(sorted_libraries):
			for new_index in range(some_index + 1, len(sorted_libraries)):
				new_library_index = sorted_libraries[new_index]
				new_new_books_list = np.setdiff1d(libraries[new_library_index]['books_ids'], libraries[library_index]['books_ids'])
				libraries[new_library_index]['books_ids'] = new_new_books_list # Setovanje onih koje nisu skenirane
				libraries[new_library_index]['number_of_books_in_library'] = len(new_new_books_list) # Izmena broja knjiga nakon setovanja
				libraries[new_library_index]['books_sum'] = sum([book_score[book_id] for book_id in new_new_books_list])
				if len(new_new_books_list) == 0:
					number_of_libraries -= 1
			to_be_sorted = sorted_libraries[some_index + 1:]
			is_sorted = sorted(to_be_sorted, key=lambda i: (max_signup_time - libraries[i]['signup_time'], libraries[i]['books_sum']))


	# Output

	outFile = open(output_files[file_index], 'w')
	outFile.write(str(number_of_libraries) + '\n')
	for library_index in sorted_libraries:
		if libraries[library_index]['number_of_books_in_library']:
			outFile.write(str(library_index) + ' ' + str(libraries[library_index]['number_of_books_in_library']) + '\n')
			outFile.write(' '.join([str(el) for el in libraries[library_index]['books_ids']]) + '\n')
