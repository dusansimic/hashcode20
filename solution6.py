import sys
import numpy as np
import math

file_number = int(sys.argv[1])

input_files = [
	'a_example.txt',
	'b_read_on.txt',
	'c_incunabula.txt',
	'd_tough_choices.txt',
	'e_so_many_books.txt',
	'f_libraries_of_the_world.txt'
]

output_files = [
	'a' + str(file_number) + '.out',
	'b' + str(file_number) + '.out',
	'c' + str(file_number) + '.out',
	'd' + str(file_number) + '.out',
	'e' + str(file_number) + '.out',
	'f' + str(file_number) + '.out'
]

def readData(file):
	f = open(file, 'r')

	line = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	number_of_books = line[0]
	number_of_libraries = line[1]
	days_limit = line[2]

	book_score = [int(el) for el in f.readline().rstrip('\n').split(' ')]

	libraries = []

	for lib_index in range(number_of_libraries):
		line1 = f.readline().rstrip('\n').split(' ')
		books_ids = [int(el) for el in f.readline().rstrip('\n').split(' ')]
		books_ids = sorted(books_ids, key=lambda i: book_score[i])
		data = {
			'library_index': lib_index,
			'number_of_books_in_library': int(line1[0]),
			'signup_time': int(line1[1]),
			'books_per_day': int(line1[2]),
			'books_ids': books_ids,
			'books_sum': sum([book_score[book_id] for book_id in books_ids])
		}
		libraries.append(data)
	f.close()
	return [number_of_books, number_of_libraries, days_limit, book_score, libraries]

def outputToFile(file, number_of_libraries, sorted_libraries):
	f = open(file, 'w')
	f.write(str(number_of_libraries) + '\n')
	for library in sorted_libraries:
		if library['number_of_books_in_library']:
			f.write(str(library['library_index']) + ' ' + str(library['number_of_books_in_library']) + '\n')
			f.write(' '.join([str(el) for el in library['books_ids']]) + '\n')
	f.close()

if __name__ == '__main__':
	for input_file, output_file in zip(input_files, output_files):
		[number_of_books, number_of_libraries, days_limit, book_score, libraries] = readData(input_file)

		# Sortiraj biblioteke po fitness funkciji
		# (books_sum * books_per_day) / signup_time
		sorted_libraries = sorted(libraries, key=lambda lib: (lib['books_sum']*lib['books_per_day'])/lib['signup_time'], reverse=True)

		time_offset = 0
		used_books = []
		for library in sorted_libraries:
			time_offset += library['signup_time']

			if time_offset > days_limit:
				number_of_libraries -= 1
				library['number_of_books_in_library'] = 0
				continue

			new_books_list = np.setdiff1d(library['books_ids'], used_books)
			library['books_ids'] = new_books_list
			library['number_of_books_in_library'] = len(new_books_list)

			# required_time = time_offset + library['signup_time'] + math.ceil(library['number_of_books_in_library']/library['books_per_day'])

			# delta_time = days_limit - required_time
			# if delta_time < 0:
			# 	doable_time = math.ceil(library['number_of_books_in_library']/library['books_per_day']) + delta_time

		outputToFile(output_file, number_of_libraries, sorted_libraries)
